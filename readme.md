# GRS is Central Reservation System (CRS) 

It is a computerized reservation software used to maintain the hotel information, room inventory and rates, to manage the reservation and process. A CRS provides hotel room rates and availability for many different distribution channels such as the GDS, IBE, OTA, 3rd party websites etc.

Our view at the design of this system has been based on cloud computing and implemented by lumen framework.

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

Documentation is [Here](http://docs.fastreserve.net/sdk/php/)
## Instaling 

* run command `composer install`

## Using API

```php

use GRS\API;
use GRS\Entities\Reserve;
use GRS\Entities\ReserveRoom;

$propertyTestId = 1416;
$token = ''; // set your token
$host = 'https://www.api.fastreserve.net';

// reserve data 
$checkIn = '2018-07-05';
$checkOut = '2018-07-08';
$bookerFirstName = 'Reza';
$bookerLastName = 'Davoodi';
$bookerPhone = '091200000000'; //optional
$bookerEmail = 'reza@gmail.com'; //optional

$api = new API( $token, $host );

// Entities\City[] $cities
$cities = $api->getCities();

// Entities\Facility[] $facilities
$facilities = $api->getFacilities();

// Entities\Property[] $properties  
$properties = $api->getProperties();

// Entities\RoomType[] $roomTypes
$roomTypes = $api->getRoomTypes( $propertyTestId );

// Entities\RoomRate[] $roomRates
$roomRates = $api->availableRooms( $propertyTestId, $checkIn, $checkOut );

// add reserve
$reserve = new Reserve();
$reserve->setPropertyId( $propertyTestId );
$reserve->setCheckIn( $checkIn );
$reserve->setCheckOut( $checkOut );
$reserve->setBooker( $bookerFirstName, $bookerLastName, $bookerPhone, $bookerEmail );

// add room
$room = new ReserveRoom();
$room->setRoomTypeId( 409958 );
$room->setRatePlanId( 1 );
$room->setCount( 1 );
$room->setAdultCount( 2 );
$room->setChildren([2,4]);
$room->setGuest('Ali', 'Davoodi');

$reserve->addRoom( $room );

// Entities\ReserveDetails $reserveDetails
$reserveDetails = $api->reserve( $reserve );

// book a reserve
// Entities\ReserveDetails $reserveDetails
$reserveDetails = $api->book( $reserveDetails->confirmation_code );

// Entities\ReserveDetails[] $myReserves
$myReserves = $api->getMyReserves();

// Entities\ReserveDetails $reserveDetails
$reserveDetails = $api->getReserveDetails( $reserveDetails->confirmation_code );

// get web hook
$url = $api->getWebHook();

// set web hook
$api->setWebHook( $url );
```
## Using Webhook
We will send the changes to your webhook URL 
```php
$webhook = new Webhook( $this->request->getJsonRawBody() );
$webhook->setReserveChangedHandler(function (ReserveDetails $reserveDetails ){
    //TODO:: handle changed reserveDetails
});
$webhook->setAvailableChangedHandler(function ( $roomRateCollection ){
    //TODO:: handle changed roomRate || receive collection of RoomRate
});
$webhook->handle();
```

## Contributing

* Saeed Torabi <saeedtrb@yahoo.com>
* Majid Ataee <eiata63dijam@gmail.com>
* Mahdi Bagheri  <mb.programmer@yahoo.com>

## License

This system is open-sourced software licensed under the [GPL V3.0 license](https://opensource.org/licenses/GPL-3.0)
<?php

namespace GRS;

use GRS\Factories\ProfileFactory;
use GRS\Factories\PropertyDetailsFactory;
use GRS\Factories\PropertySuggestionFactory;
use GRS\Enums\HttpStatusCodes;
use GRS\Exceptions\BaseRuntimeException;
use GRS\Exceptions\HttpException;
use GRS\Entities\Reserve;
use GRS\Exceptions\RequestException;
use GRS\Exceptions\TokenException;
use GRS\Exceptions\ValidationException;
use GRS\Factories\ErrorFactory;
use GRS\Factories\FacilityFactory;
use GRS\Factories\CityFactory;
use GRS\Factories\PropertyFactory;
use GRS\Factories\ReserveDetailsResponseFactory;
use GRS\Factories\SuggestionFactory;
use GRS\Factories\TransactionFactory;
use GRS\Factories\ReserveDetailsFactory;
use GRS\Factories\RoomRateFactory;
use GRS\Factories\RoomTypeFactory;

class API
{
    protected $token;
    protected $host;
    protected $version;

    /**
     * API constructor.
     * @param string $host
     * @param string $token
     * @param string $version
     */
    public function __construct($token, $host, $version = 'v1')
    {
        if (!extension_loaded('curl')) {
            throw new BaseRuntimeException('cURL is required!!!');
        }

        $this->host = $host;
        $this->token = $token;
        $this->version = $version;
    }

    /**
     * @param string $methodName
     * @return string
     */
    protected function getPath($methodName)
    {
        return $this->host . '/' . $this->version . '/' . $methodName;
    }

    /**
     * @param $requestType
     * @param $methodName
     * @param null $data
     * @param null $url_post_data
     * @return \stdClass
     */
    private function _call($requestType, $methodName, $data = null)
    {
        $url = $this->getPath($methodName);

        $headers = array(
            'Client-Token: ' . $this->token,
            'Content-Type: application/json'
        );

        $handle = curl_init();

        switch (strtolower($requestType)) {
            case "post":
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            default:
                if (!is_null($data)) {
                    $url .= '?' . http_build_query($data);
                }
                break;
        }
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($handle);
        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        $curl_error_code = curl_errno($handle);
        $curl_error = curl_error($handle);

        if ($curl_error_code) {
            throw new RequestException($curl_error, $curl_error_code);
        }
        $result = json_decode($response);
        switch ($code) {
            case HttpStatusCodes::FORBIDDEN;
                throw new TokenException('GRS API token is not valid!!', HttpStatusCodes::FORBIDDEN);
                break;
            case HttpStatusCodes::UNPROCESSABLE_ENTITY:
                throw new ValidationException((new ErrorFactory())->makeFromArray($result->errors));
                break;
            case HttpStatusCodes::OK;
                return $result->value;
                break;
            default:
                throw new HttpException('http request has error', $code);
                break;
        }
    }

    /**
     * @return Entities\City[]
     */
    public function getCities()
    {
        $result = $this->_call('GET', 'cities');
        return (new CityFactory())->makeFromArray($result->cities);
    }

    /**
     * @return Entities\Facility[]
     */
    public function getFacilities()
    {
        $result = $this->_call('GET', 'facilities');
        return (new FacilityFactory())->makeFromArray($result->facilities);
    }

    /**
     * @param null $offset optional
     * @param null $count optional
     * @return Entities\Property[]
     */
    public function getProperties( $offset = null, $count = null )
    {
        $result = $this->_call('GET', 'properties', ['offset' => $offset, 'count' => $count]);
        return (new PropertyFactory())->makeFromArray($result->properties);
    }

    /**
     * @param null $offset
     * @param null $count
     * @return Entities\ReserveDetails[]
     */
    public function getReserves($offset = null, $count = null )
    {
        $result = $this->_call('GET', 'reserves', ['offset' => $offset, 'count' => $count]);
        return (new ReserveDetailsFactory())->makeFromArray($result->reserves);
    }

    /**
     * @param $propertyId
     * @return Entities\RoomType[]
     */
    public function getRoomTypes($propertyId)
    {
        $result = $this->_call('GET', 'room-types', [
            'property_id' => $propertyId
        ]);
        return (new RoomTypeFactory())->makeFromArray($result->room_types);
    }

    /**
     * @param $propertyId
     * @param $checkIn
     * @param $checkOut
     * @return Entities\RoomRate[]
     */
    public function availableRooms($propertyId, $checkIn, $checkOut)
    {
        $result = $this->_call('GET', 'available-rooms', [
            'property_id' => $propertyId,
            'check_in' => $checkIn,
            'check_out' => $checkOut
        ]);
        return (new RoomRateFactory())->makeFromArray($result->rooms);
    }

    /**
     * @param string $checkIn
     * @param string $checkOut
     * @param int $adultsCount
     * @param int $cityId
     * @param int $propertyId optional
     * @param null $children optional
     * @param null $roomsCount optional
     * @return Entities\Suggestion[]
     */
    public function suggestion($checkIn, $checkOut, $adultsCount, $cityId, $propertyId = null, $children = null, $roomsCount = null)
    {
        $args = [
            'check_in' => $checkIn,
            'check_out' => $checkOut,
            'adults_count' => $adultsCount,
            'city_id' => $cityId,
            'property_id' => $propertyId,
            'children' => $children,
            'rooms_count' => $roomsCount
        ];

        $result = $this->_call('GET', 'suggestion', $args);

        return (new SuggestionFactory())->makeFromArray($result->suggestions);
    }

    /**
     * @param Reserve $reserve
     * @return Entities\ReserveDetails
     */
    public function reserve(Reserve $reserve)
    {
        $result = $this->_call('POST', 'reserve', $reserve->toArray());
        return (new ReserveDetailsFactory())->make($result->reserve);
    }
    /**
     * @param $confirmationCode
     * @return Entities\ReserveDetails
     */
    public function extendedExpiredTime($confirmationCode)
    {
        $result = $this->_call('POST', 'extended-expired-time/' . $confirmationCode);
        return (new ReserveDetailsFactory())->make($result->reserve);
    }
    /**
     * @param $confirmationCode
     * @return Entities\ReserveDetails
     */
    public function book($confirmationCode)
    {
        $result = $this->_call('POST', 'book', ['confirmation_code' => $confirmationCode]);
        return (new ReserveDetailsFactory())->make($result->reserve);
    }

    /**
     * @param $confirmationCode
     * @return Entities\ReserveDetails
     */
    public function cancel($confirmationCode)
    {
        $result = $this->_call('POST', 'cancel', ['confirmation_code' => $confirmationCode]);
        return (new ReserveDetailsFactory())->make($result->reserve);
    }

    /**
     * @param Reserve $reserve
     * @param $confirmationCode
     * @return Entities\ReserveDetails
     */
    public function modifyReserve(Reserve $reserve, $confirmationCode)
    {
        $result = $this->_call('POST', 'modify/'.$confirmationCode , $reserve->toArray());
        return (new ReserveDetailsFactory())->make($result->reserve);
    }

    /**
     * @param Reserve $reserve
     * @param $confirmationCode
     * @return Entities\ReserveDetails
     */
    public function acceptModifyReserve(Reserve $reserve, $confirmationCode)
    {
        $result = $this->_call('POST', 'accept-modify/'.$confirmationCode , $reserve->toArray());
        return (new ReserveDetailsFactory())->make($result->reserve);
    }

    /**
     * @param $confirmationCode
     * @return Entities\ReserveDetails
     */
    public function acceptCancel($confirmationCode)
    {
        $result = $this->_call('POST', 'accept-cancel/'.$confirmationCode);
        return (new ReserveDetailsFactory())->make($result->reserve);
    }

    /**
     * @param $confirmationCode
     * @return Entities\ReserveDetails
     */
    public function rejectCancel($confirmationCode)
    {
        $result = $this->_call('POST', 'reject-cancel/'.$confirmationCode);
        return (new ReserveDetailsFactory())->make($result->reserve);
    }

    /**
     * @return Entities\ReserveDetails[]
     */
    public function getMyReserves()
    {
        $result = $this->_call('GET', 'reserves');
        return (new ReserveDetailsFactory())->makeFromArray($result->reserves);
    }

    /**
     * @param $confirmationCode
     * @return Entities\ReserveDetailsResponse
     */
    public function getReserveDetails($confirmationCode)
    {
        $result = $this->_call('GET', 'reserve-details', ['confirmation_code' => $confirmationCode]);
        return (new ReserveDetailsResponseFactory())->make($result);
    }

    /**
     * @return Entities\Transaction[]
     */
    public function getTransactions()
    {
        $result = $this->_call('GET', 'transactions');
        return (new TransactionFactory())->makeFromArray($result->transactions);
    }

    /**
     * @return Entities\Profile
     */
    public function getProfile()
    {
        $result = $this->_call('GET', 'profile');
        return (new ProfileFactory())->make($result->profile);
    }

    /**
     * @return String
     */
    public function getWebHook()
    {
        $result = $this->_call('GET', 'web-hook');
        return $result->web_hook;
    }

    /**
     * @param $url
     * @return bool
     */
    public function setWebHook($url)
    {
        $this->_call('POST', 'web-hook', ['web_hook' => $url]);
        return true;
    }
}
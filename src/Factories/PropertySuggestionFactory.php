<?php
namespace GRS\Factories;

use GRS\Entities\PropertySuggestion;

class PropertySuggestionFactory
{
    /**
     * @param $entity
     * @return PropertySuggestion
     */
    public function make( $entity ){
        $property = new PropertySuggestion();

        $property->setPropertyId( $entity->property_id );
        $property->setPropertyName( $entity->property_name );
        $property->setRooms( ( new SuggestionRoomFactory() )->makeFromArray( $entity->rooms ) );

        return $property;
    }

    /**
     * @param $entities
     * @return PropertySuggestion[] $properties
     */
    public function makeFromArray($entities){
        $properties = [];
        foreach ( $entities as $entity ){
            $properties[] = $this->make( $entity );
        }

        return $properties;
    }
}
<?php

namespace GRS\Factories;

use GRS\Entities\RoomRate;

class RoomRateFactory
{
    /**
     * @param $entity
     * @return RoomRate
     */
    public function make($entity)
    {
        $roomRate = new RoomRate();

        $roomRate->setPropertyId(isset($entity->property_id) ? $entity->property_id : null);
        $roomRate->setRoomTypeId(isset($entity->room_type_id) ? $entity->room_type_id : null);
        $roomRate->setRoomTypeName(isset($entity->room_type_name) ? $entity->room_type_name : null);
        $roomRate->setRatePlans((new RatePlanFactory())->makeFromArray($entity->rate_plans));

        return $roomRate;
    }

    /**
     * @param $entities
     * @return RoomRate[] $roomRateCollection
     */
    public function makeFromArray($entities)
    {
        $roomRateCollection = [];
        foreach ($entities as $entity) {
            $roomRateCollection[] = $this->make($entity);
        }

        return $roomRateCollection;
    }

}
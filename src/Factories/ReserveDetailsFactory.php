<?php
namespace GRS\Factories;

use GRS\Entities\ReserveDetails;

class ReserveDetailsFactory
{
    /**
     * @param $entity
     * @return ReserveDetails
     */
    public function make( $entity ){
        $reserveDetails = new ReserveDetails();

        $reserveDetails->setPropertyId( $entity->property_id );
        $reserveDetails->setCheckIn( $entity->check_in );
        $reserveDetails->setCheckOut( $entity->check_out );
        $reserveDetails->setBookerFirstName( $entity->booker_first_name );
        $reserveDetails->setBookerLastName( $entity->booker_last_name );
        $reserveDetails->setBookerPhone( $entity->booker_phone );
        $reserveDetails->setBookerEmail( $entity->booker_email );
        $reserveDetails->setVehicle( $entity->vehicle );
        $reserveDetails->setVehicleNumber( $entity->vehicle_number );
        $reserveDetails->setState( $entity->state );
        $reserveDetails->setStatus( $entity->status );
        $reserveDetails->setConfirmationCode( $entity->confirmation_code );
        $reserveDetails->setPropertyConfirmationCode( $entity->property_confirmation_code );
        $reserveDetails->setTotalCancellationFee( $entity->total_cancellation_fee );
        $reserveDetails->setTotalModificationFee( $entity->total_modification_fee );
        $reserveDetails->setTotalPrice( $entity->total_price );
        $reserveDetails->setTotalDailyPrice( $entity->total_daily_price );
        $reserveDetails->setTotalSalesPrice( $entity->total_sales_price );
        $reserveDetails->setDescription( $entity->description );
        $reserveDetails->setCreateDate( $entity->create_date );
        $reserveDetails->setExpireDate( $entity->expire_date );
        $reserveDetails->setCancelDate( $entity->cancel_date );
        $reserveDetails->setDefiniteDate( $entity->definite_date );
        $reserveDetails->setRooms( ( new ReserveRoomDetailsFactory() )->makeFromArray( $entity->rooms ) );

        return $reserveDetails;
    }

    /**
     * @param $entities
     * @return ReserveDetails[] $ReserveDetailsCollection
     */
    public function makeFromArray($entities){
        $ReserveDetailsCollection = [];
        foreach ( $entities as $entity ){
            $ReserveDetailsCollection[] = $this->make( $entity );
        }

        return $ReserveDetailsCollection;
    }

}
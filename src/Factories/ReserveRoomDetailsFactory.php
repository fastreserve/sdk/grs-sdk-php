<?php
namespace GRS\Factories;

use GRS\Entities\ReserveRoomDetails;

class ReserveRoomDetailsFactory
{
    /**
     * @param $entity
     * @return ReserveRoomDetails
     */
    public function make( $entity ){
        $reserveRoomDetails = new ReserveRoomDetails();

        $reserveRoomDetails->setRoomTypeId( $entity->room_type_id );
        $reserveRoomDetails->setRatePlanId( $entity->rate_plan_id );
        $reserveRoomDetails->setCount( $entity->count );
        $reserveRoomDetails->setAdultCount( $entity->adult_count );
        $reserveRoomDetails->setChildren( $entity->children );
        $reserveRoomDetails->setGuestFirstName( $entity->guest_first_name );
        $reserveRoomDetails->setGuestLastName( $entity->guest_last_name );
        $reserveRoomDetails->setGuestPhone( $entity->guest_phone );
        $reserveRoomDetails->setGuestEmail( $entity->guest_email );
        $reserveRoomDetails->setGuestNationalCode( $entity->guest_national_code );
        $reserveRoomDetails->setGuestPassportNumber( $entity->guest_passport_number );
        $reserveRoomDetails->setGuestCountryId( $entity->guest_country_id );
        $reserveRoomDetails->setGuestCityId( $entity->guest_city_id );
        $reserveRoomDetails->setGuests($entity->guests ? (new ReserveRoomGuestFactory())->makeFromArray( $entity->guests) : []);
        $reserveRoomDetails->setPrices( ( new RatePlanPriceFactory() )->makeFromArray( $entity->prices ));
        $reserveRoomDetails->setTotalCancellationFee( $entity->total_cancellation_fee );
        $reserveRoomDetails->setTotalModificationFee( $entity->total_modification_fee );
        $reserveRoomDetails->setTotalPrice( $entity->total_price );
        $reserveRoomDetails->setTotalDailyPrice( $entity->total_daily_price );
        $reserveRoomDetails->setTotalSalesPrice( $entity->total_sales_price );

        return $reserveRoomDetails;
    }

    /**
     * @param $entities
     * @return ReserveRoomDetails[] $ReserveRoomDetailsCollection
     */
    public function makeFromArray($entities){
        $ReserveRoomDetailsCollection = [];
        foreach ( $entities as $entity ){
            $ReserveRoomDetailsCollection[] = $this->make( $entity );
        }

        return $ReserveRoomDetailsCollection;
    }
}
<?php

namespace GRS\Factories;


use GRS\Entities\Profile;

class ProfileFactory
{
    /**
     * @param $entity
     * @return Profile
     */
    public function make( $entity ){
        $profile = new Profile();

        $profile->setName($entity->name);
        $profile->setCreditAccountAmount($entity->credit_account_amount);
        $profile->setDebitAccountAmount($entity->debit_account_amount);
        $profile->setWebHook($entity->web_hook);

        return $profile;
    }
}
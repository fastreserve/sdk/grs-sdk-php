<?php

namespace GRS\Factories;


use GRS\Entities\Transaction;

class TransactionFactory
{
    /**
     * @param $entity
     * @return Transaction
     */
    public function make( $entity ){
        $transaction = new Transaction();

        $transaction->setReserveConfirmationCode( $entity->reserve_confirmation_code );
        $transaction->setType( $entity->type );
//        $transaction->setPaidDate( $entity->paid_date ? new \DateTime($entity->paid_date) : null );
        $transaction->setAmount( $entity->amount );
        $transaction->setDescription( $entity->description );
//        $transaction->setCreatedAt( new \DateTime($entity->created_at) );

        return $transaction;
    }

    /**
     * @param $entities
     * @return Transaction[] receipts
     */
    public function makeFromArray($entities){
        $transactions = [];
        foreach ( $entities as $entity ){
            $transactions[] = $this->make( $entity );
        }

        return $transactions;
    }

}
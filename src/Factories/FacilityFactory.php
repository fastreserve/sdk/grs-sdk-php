<?php
namespace GRS\Factories;

use GRS\Entities\Facility;

class FacilityFactory
{
    /**
     * @param $entity
     * @return Facility
     */
    public function make( $entity ){
        $facility = new Facility();

        $facility->setId( $entity->id );
        $facility->setName( $entity->name );
        $facility->setGroupId( $entity->group_id );
        $facility->setGroupName( $entity->group_name );

        return $facility;
    }

    /**
     * @param $entities
     * @return Facility[] $facilities
     */
    public function makeFromArray($entities){
        $facilities = [];
        foreach ( $entities as $entity ){
            $facilities[] = $this->make( $entity );
        }

        return $facilities;
    }

}
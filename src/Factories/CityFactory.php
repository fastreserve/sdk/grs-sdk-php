<?php

namespace GRS\Factories;


use GRS\Entities\City;

class CityFactory
{
    /**
     * @param $entity
     * @return City
     */
    public function make( $entity ){
        $city = new City();

        $city->setId($entity->id);
        $city->setName($entity->name);
        $city->setProvinceId($entity->province_id);
        $city->setProvinceName($entity->province_name);
        $city->setCountryId($entity->country_id);
        $city->setCountryName( $entity->country_name );

        return $city;
    }

    /**
     * @param $entities
     * @return City[] cities
     */
    public function makeFromArray($entities){
        $cities = [];
        foreach ( $entities as $entity ){
            $cities[] = $this->make( $entity );
        }

        return $cities;
    }

}
<?php
namespace GRS\Factories;

use GRS\Entities\Property;
use GRS\Entities\PropertyDetails;

class PropertyDetailsFactory
{
    /**
     * @param $entity
     * @return PropertyDetails
     */
    public function make( $entity ){
        $property = new PropertyDetails();

        $property->setId( $entity->id );
        $property->setName( $entity->name );
        $property->setType( $entity->type );
        $property->setStar( $entity->star );
        $property->setGrade( $entity->grade );
        $property->setProvinceId( $entity->province_id );
        $property->setCityId( $entity->city_id );
        $property->setLatitude( $entity->latitude );
        $property->setLongitude( $entity->longitude );
        $property->setAddress( $entity->address );
        $property->setRoomsCount( $entity->rooms_count );
        $property->setFacilities( ( new FacilityDetailsFactory() )->makeFromArray( $entity->facilities ) );
        $property->setRoomTypes( ( new RoomTypeFactory() )->makeFromArray( $entity->room_types ) );
        $property->setImages( ( new FileFactory() )->makeFromArray( $entity->images ) );
        $property->setDescription( $entity->description );

        return $property;
    }

    /**
     * @param $entities
     * @return Property[] $properties
     */
    public function makeFromArray($entities){
        $properties = [];
        foreach ( $entities as $entity ){
            $properties[] = $this->make( $entity );
        }

        return $properties;
    }
}
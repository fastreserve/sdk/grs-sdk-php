<?php

namespace GRS\Factories;

use GRS\Entities\RatePlanPrice;

class RatePlanPriceFactory
{
    /**
     * @param $entity
     * @return RatePlanPrice
     */
    public function make($entity)
    {
        $price = new RatePlanPrice();

        $price->setDay(isset($entity->day) ? $entity->day : null);
        $price->setInventory(isset($entity->inventory) ? $entity->inventory : null);
        $price->setRackRate(isset($entity->rack_rate) ? $entity->rack_rate : null);
        $price->setDailyRate(isset($entity->daily_rate) ? $entity->daily_rate : null);
        $price->setGrsRate(isset($entity->grs_rate) ? $entity->grs_rate : null);
        $price->setBabyCotRackRate(isset($entity->baby_cot_rack_rate) ? $entity->baby_cot_rack_rate : null);
        $price->setBabyCotDailyRate(isset($entity->baby_cot_daily_rate) ? $entity->baby_cot_daily_rate : null);
        $price->setBabyCotGrsRate(isset($entity->baby_cot_grs_rate) ? $entity->baby_cot_grs_rate : null);
        $price->setExtendBedRackRate(isset($entity->extend_bed_rack_rate) ? $entity->extend_bed_rack_rate : null);
        $price->setExtendBedDailyRate(isset($entity->extend_bed_daily_rate) ? $entity->extend_bed_daily_rate : null);
        $price->setExtendBedGrsRate(isset($entity->extend_bed_grs_rate) ? $entity->extend_bed_grs_rate : null);
        $price->setReservationState(isset($entity->reservation_state) ? $entity->reservation_state : null);
        $price->setMinStay(isset($entity->min_stay) ? $entity->min_stay : null);
        $price->setMaxStay(isset($entity->max_stay) ? $entity->max_stay : null);
        $price->setClosed(isset($entity->closed) ? $entity->closed : null);

        return $price;
    }

    /**
     * @param $entities
     * @return RatePlanPrice[] $prices
     */
    public function makeFromArray($entities)
    {
        $prices = [];
        foreach ($entities as $entity) {
            $prices[] = $this->make($entity);
        }

        return $prices;
    }
}
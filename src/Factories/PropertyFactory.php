<?php
namespace GRS\Factories;

use GRS\Entities\Property;

class PropertyFactory
{
    /**
     * @param $entity
     * @return Property
     */
    public function make( $entity ){
        $property = new Property();

        $property->setId( $entity->id );
        $property->setName( $entity->name );
        $property->setType( $entity->type );
        $property->setStar( $entity->star );
        $property->setGrade( $entity->grade );
        $property->setProvinceId( $entity->province_id );
        $property->setCityId( $entity->city_id );
        $property->setLatitude( $entity->latitude );
        $property->setLongitude( $entity->longitude );
        $property->setAddress( $entity->address );
        $property->setRoomsCount( $entity->rooms_count );
        $property->setFacilities( ( new FacilityDetailsFactory() )->makeFromArray( $entity->facilities ) );
        $property->setImages( ( new FileFactory() )->makeFromArray( $entity->images ) );
        $property->setDescription( $entity->description );

        return $property;
    }

    /**
     * @param $entities
     * @return Property[] $properties
     */
    public function makeFromArray($entities){
        $properties = [];
        foreach ( $entities as $entity ){
            $properties[] = $this->make( $entity );
        }

        return $properties;
    }
}
<?php
namespace GRS\Factories;

use GRS\Entities\FacilityDetails;

class FacilityDetailsFactory
{
    /**
     * @param $entity
     * @return FacilityDetails
     */
    public function make( $entity ){
        $facilityDetails = new FacilityDetails();

        $facilityDetails->setId( $entity->id );
        $facilityDetails->setName( $entity->name );
        $facilityDetails->setGroupId( $entity->group_id );
        $facilityDetails->setDescription( $entity->description );

        return $facilityDetails;
    }

    /**
     * @param $entities
     * @return FacilityDetails[] $facilityDetailsCollection
     */
    public function makeFromArray($entities){
        $facilityDetailsCollection = [];
        foreach ( $entities as $entity ){
            $facilityDetailsCollection[] = $this->make( $entity );
        }

        return $facilityDetailsCollection;
    }
}
<?php

namespace GRS\Factories;

use GRS\Entities\ReserveRoomGuest;

class ReserveRoomGuestFactory
{
    /**
     * @param $entity
     * @return ReserveRoomGuest
     */
    public function make($entity)
    {
        $reserveRoomGuest = new ReserveRoomGuest();

        $reserveRoomGuest->setFirstName(isset($entity->first_name) ? $entity->first_name : null);
        $reserveRoomGuest->setLastName(isset($entity->last_name) ? $entity->last_name : null);
        $reserveRoomGuest->setPhone(isset($entity->phone) ? $entity->phone : null);
        $reserveRoomGuest->setEmail(isset($entity->email) ? $entity->email : null);
        $reserveRoomGuest->setNationalCode(isset($entity->national_code) ? $entity->national_code : null);
        $reserveRoomGuest->setPassportNumber(isset($entity->passport_number) ? $entity->passport_number : null);
        $reserveRoomGuest->setCountryId(isset($entity->country_id) ? $entity->country_id : null);
        $reserveRoomGuest->setCityId(isset($entity->city_id) ? $entity->city_id : null);

        return $reserveRoomGuest;
    }

    /**
     * @param  $entities
     * @return ReserveRoomGuest[] $reserveRoomGuest
     */
    public function makeFromArray($entities)
    {
        $reserveRoomGuests = [];
        foreach ($entities as $entity) {
            $reserveRoomGuests[] = $this->make($entity);
        }

        return $reserveRoomGuests;
    }
}
<?php
namespace GRS\Factories;

use GRS\Entities\RatePlan;

class RatePlanDetailsFactory
{
    /**
     * @param $entity
     * @return RatePlan
     */
    public function make( $entity ){
        $ratePlan = new RatePlan();

        $ratePlan->setId( $entity->id );
        $ratePlan->setName( $entity->name );
        $ratePlan->setMealTypeIncluded( $entity->meal_type_included );
        $ratePlan->setFoodBoardType( $entity->food_board_type );
        $ratePlan->setBreakfastRate( $entity->breakfast_rate );
        $ratePlan->setHalfBoardRate( $entity->half_board_rate );
        $ratePlan->setFullBoardRate( $entity->full_board_rate );
        $ratePlan->setCancelable( $entity->cancelable );
        $ratePlan->setSleeps( $entity->sleeps );
        $ratePlan->setFacilities( ( new FacilityDetailsFactory )->makeFromArray( $entity->facilities ) );

        return $ratePlan;
    }

    /**
     * @param $entities
     * @return RatePlan[] $ratePlans
     */
    public function makeFromArray($entities){
        $ratePlans = [];
        foreach ( $entities as $entity ){
            $ratePlans[] = $this->make( $entity );
        }

        return $ratePlans;
    }
}
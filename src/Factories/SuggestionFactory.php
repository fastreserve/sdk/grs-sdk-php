<?php

namespace GRS\Factories;

use GRS\Entities\Suggestion;

class SuggestionFactory
{
    /**
     * @param $entity
     * @return Suggestion
     */
    public function make($entity)
    {
        $suggestion = new Suggestion();

        $suggestion->setPropertyId(isset($entity->property_id) ? $entity->property_id : null);
        $suggestion->setPropertyName(isset($entity->property_name) ? $entity->property_name : null);
        $suggestion->setRooms((new SuggestionRoomFactory())->makeFromArray($entity->rooms));

        return $suggestion;
    }

    /**
     * @param $entities
     * @return Suggestion[] $suggestionCollection
     */
    public function makeFromArray($entities)
    {
        $suggestion = [];
        foreach ($entities as $entity) {
            $suggestion[] = $this->make($entity);
        }

        return $suggestion;
    }

}
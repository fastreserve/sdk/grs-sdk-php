<?php
namespace GRS\Factories;

use GRS\Entities\ReserveDetailsResponse;


class ReserveDetailsResponseFactory
{
    /**
     * @param $entity
     * @return ReserveDetailsResponse
     */
    public function make( $entity ){
        $reserveDetailsResponse = new ReserveDetailsResponse();

        $reserveDetailsResponse->setReserve( (new ReserveDetailsFactory())->make($entity->reserve));
        $reserveDetailsResponse->setRelatedReserves((new ReserveDetailsFactory())->makeFromArray($entity->related_reserves));

        return $reserveDetailsResponse;
    }

    /**
     * @param $entities
     * @return ReserveDetailsResponse[] $ReserveDetailsResponseCollection
     */
    public function makeFromArray($entities){
        $ReserveDetailsResponseCollection = [];
        foreach ( $entities as $entity ){
            $ReserveDetailsResponseCollection[] = $this->make( $entity );
        }

        return $ReserveDetailsResponseCollection;
    }
}
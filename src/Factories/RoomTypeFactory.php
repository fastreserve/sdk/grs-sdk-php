<?php
namespace GRS\Factories;

use GRS\Entities\RoomType;

class RoomTypeFactory
{
    /**
     * @param $entity
     * @return RoomType
     */
    public function make( $entity ){
        $roomType = new RoomType();

        $roomType->setId( $entity->id );
        $roomType->setName( $entity->name );
        $roomType->setType( $entity->type );
        $roomType->setCapacity( $entity->capacity );
        $roomType->setExtraCapacity( $entity->extra_capacity );
        $roomType->setSingleBedCount( $entity->single_bed_count );
        $roomType->setDoubleBedCount( $entity->double_bed_count );
        $roomType->setSofaBedCount( $entity->sofa_bed_count );
        $roomType->setOutOfService( $entity->out_of_service );
        $roomType->setFacilities( ( new FacilityDetailsFactory() )->makeFromArray( $entity->facilities ) );
        $roomType->setRatePlans( ( new RatePlanDetailsFactory() )->makeFromArray( $entity->rate_plans ) );
        $roomType->setDescription( $entity->description );

        return $roomType;
    }

    /**
     * @param $entities
     * @return RoomType[] $roomTypes
     */
    public function makeFromArray($entities){
        $roomTypes = [];
        foreach ( $entities as $entity ){
            $roomTypes[] = $this->make( $entity );
        }

        return $roomTypes;
    }
}
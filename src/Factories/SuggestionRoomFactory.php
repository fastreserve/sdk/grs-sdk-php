<?php

namespace GRS\Factories;

use GRS\Entities\SuggestionRoom;

class SuggestionRoomFactory
{
    /**
     * @param $entity
     * @return SuggestionRoom
     */
    public function make($entity)
    {
        $suggestionRoom = new SuggestionRoom();

        $suggestionRoom->setRoomTypeId(isset($entity->room_type_id) ? $entity->room_type_id : null);
        $suggestionRoom->setRoomTypeName(isset($entity->room_type_name) ? $entity->room_type_name : null);
        $suggestionRoom->setRoomTypeCapacity(isset($entity->room_type_capacity) ? $entity->room_type_capacity : null);
        $suggestionRoom->setRoomTypeExtraCapacity(isset($entity->room_type_extra_capacity) ? $entity->room_type_extra_capacity : null);
        $suggestionRoom->setRatePlans((new RatePlanFactory())->makeFromArray($entity->rate_plans));

        return $suggestionRoom;
    }

    /**
     * @param $entities
     * @return SuggestionRoom[] $suggestionRoomCollection
     */
    public function makeFromArray($entities)
    {
        $suggestionRoom = [];
        foreach ($entities as $entity) {
            $suggestionRoom[] = $this->make($entity);
        }

        return $suggestionRoom;
    }

}
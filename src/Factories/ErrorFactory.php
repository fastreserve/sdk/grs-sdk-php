<?php
namespace GRS\Factories;

use GRS\Entities\Error;

class ErrorFactory
{
    /**
     * @param $entity
     * @return Error
     */
    public function make( $entity ){
        $error = new Error();

        $error->setName($entity->name);
        $error->setMessage($entity->message);

        return $error;
    }

    /**
     * @param $entities
     * @return Error[] $errors
     */
    public function makeFromArray($entities){
        $errors = [];
        foreach ( $entities as $entity ){
            $errors[] = $this->make( $entity );
        }

        return $errors;
    }

}
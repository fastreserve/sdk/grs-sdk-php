<?php

namespace GRS\Factories;


use GRS\Entities\File;

class FileFactory
{
    /**
     * @param $entity
     * @return File $file
     */
    public function make( $entity ){
        $file = new File();

        $file->setName($entity->name);
        $file->setCaption($entity->caption);
        $file->setUrl($entity->url);

        return $file;
    }

    /**
     * @param $entities
     * @return File[] $files
     */
    public function makeFromArray($entities){
        $files = [];
        foreach ( $entities as $entity ){
            $files[] = $this->make( $entity );
        }

        return $files;
    }

}
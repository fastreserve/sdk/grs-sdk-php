<?php
namespace GRS\Entities;

use GRS\Factories\ReserveRoomGuestFactory;

class ReserveRoomDetails
{
    private $roomTypeId;
    private $ratePlanId;
    private $count;
    private $adultCount;
    private $children;
    private $guestFirstName;
    private $guestLastName;
    private $guestPhone;
    private $guestEmail;
    private $guestNationalCode;
    private $guestPassportNumber;
    private $guestCountryId;
    private $guestCityId;
    /**
     * @var ReserveRoomGuest[] $guest
     */
    private $guests;

    /**
     * @var RatePlanPrice[] $prices
     */
    private $prices;
    private $totalCancellationFee;
    private $totalModificationFee;
    private $totalPrice;
    private $totalDailyPrice;
    private $totalSalesPrice;

    /**
     * @return int
     */
    public function getRoomTypeId()
    {
        return $this->roomTypeId;
    }

    /**
     * @param int $roomTypeId
     */
    public function setRoomTypeId($roomTypeId)
    {
        $this->roomTypeId = $roomTypeId;
    }

    /**
     * @return int
     */
    public function getRatePlanId()
    {
        return $this->ratePlanId;
    }

    /**
     * @param int $ratePlanId
     */
    public function setRatePlanId($ratePlanId)
    {
        $this->ratePlanId = $ratePlanId;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getAdultCount()
    {
        return $this->adultCount;
    }

    /**
     * @param int $adultCount
     */
    public function setAdultCount($adultCount)
    {
        $this->adultCount = $adultCount;
    }

    /**
     * @return int[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param int[] $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return string
     */
    public function getGuestFirstName()
    {
        return $this->guestFirstName;
    }

    /**
     * @param string $guestFirstName
     */
    public function setGuestFirstName($guestFirstName)
    {
        $this->guestFirstName = $guestFirstName;
    }

    /**
     * @return string
     */
    public function getGuestLastName()
    {
        return $this->guestLastName;
    }

    /**
     * @param string $guestLastName
     */
    public function setGuestLastName($guestLastName)
    {
        $this->guestLastName = $guestLastName;
    }

    /**
     * @return string
     */
    public function getGuestPhone()
    {
        return $this->guestPhone;
    }

    /**
     * @param string $guestPhone
     */
    public function setGuestPhone($guestPhone)
    {
        $this->guestPhone = $guestPhone;
    }

    /**
     * @return string
     */
    public function getGuestEmail()
    {
        return $this->guestEmail;
    }

    /**
     * @param string $guestEmail
     */
    public function setGuestEmail($guestEmail)
    {
        $this->guestEmail = $guestEmail;
    }

    /**
     * @return string
     */
    public function getGuestNationalCode()
    {
        return $this->guestNationalCode;
    }

    /**
     * @param string $guestNationalCode
     */
    public function setGuestNationalCode($guestNationalCode)
    {
        $this->guestNationalCode = $guestNationalCode;
    }

    /**
     * @return string
     */
    public function getGuestPassportNumber()
    {
        return $this->guestPassportNumber;
    }

    /**
     * @param string $guestPassportNumber
     */
    public function setGuestPassportNumber($guestPassportNumber)
    {
        $this->guestPassportNumber = $guestPassportNumber;
    }

    /**
     * @return int
     */
    public function getGuestCountryId()
    {
        return $this->guestCountryId;
    }

    /**
     * @param int $guestCountryId
     */
    public function setGuestCountryId($guestCountryId)
    {
        $this->guestCountryId = $guestCountryId;
    }

    /**
     * @return int
     */
    public function getGuestCityId()
    {
        return $this->guestCityId;
    }

    /**
     * @param int $guestCityId
     */
    public function setGuestCityId($guestCityId)
    {
        $this->guestCityId = $guestCityId;
    }

    /**
     * @return ReserveRoomGuest[]
     */
    public function getGuests()
    {
        return $this->guests;
    }

    /**
     * @param ReserveRoomGuest[] $guests
     */
    public function setGuests($guests)
    {
        $this->guests = $guests;
    }

    /**
     * @return RatePlanPrice[]
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param RatePlanPrice[] $prices
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
    }

    /**
     * @return mixed
     */
    public function getTotalCancellationFee()
    {
        return $this->totalCancellationFee;
    }

    /**
     * @param mixed $totalCancellationFee
     */
    public function setTotalCancellationFee($totalCancellationFee)
    {
        $this->totalCancellationFee = $totalCancellationFee;
    }

    /**
     * @return mixed
     */
    public function getTotalModificationFee()
    {
        return $this->totalModificationFee;
    }

    /**
     * @param mixed $totalModificationFee
     */
    public function setTotalModificationFee($totalModificationFee)
    {
        $this->totalModificationFee = $totalModificationFee;
    }

    /**
     * @return int
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param int $totalPrice
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return integer
     */
    public function getTotalDailyPrice()
    {
        return $this->totalDailyPrice;
    }

    /**
     * @param integer $totalDailyPrice
     */
    public function setTotalDailyPrice($totalDailyPrice)
    {
        $this->totalDailyPrice = $totalDailyPrice;
    }

    /**
     * @return int
     */
    public function getTotalSalesPrice()
    {
        return $this->totalSalesPrice;
    }

    /**
     * @param int $totalSalesPrice
     */
    public function setTotalSalesPrice($totalSalesPrice)
    {
        $this->totalSalesPrice = $totalSalesPrice;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $properties = get_object_vars( $this );

        $properties['guests'] = isset($properties['guests']) && is_array($properties['guests']) ? $properties['guests'] : [];
        foreach ( $this->getGuests() as $guest ){
            $properties['guests'][] = $guest->toArray();
        }

        $properties['prices'] = [];
        foreach ( $this->getPrices() as $rate ){
            $properties['prices'][] = $rate->toArray();
        }
        return $properties;
    }
}
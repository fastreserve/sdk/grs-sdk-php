<?php
namespace GRS\Entities;

class ReserveDetailsResponse
{
    private $reserve;
    private $relatedReserves;

    /**
     * @return ReserveDetails
     */
    public function getReserve()
    {
        return $this->reserve;
    }

    /**
     * @param mixed $reserve
     */
    public function setReserve($reserve)
    {
        $this->reserve = $reserve;
    }

    /**
     * @return ReserveDetails[]
     */
    public function getRelatedReserves()
    {
        return $this->relatedReserves;
    }

    /**
     * @param mixed $relatedReserves
     */
    public function setRelatedReserves($relatedReserves)
    {
        $this->relatedReserves = $relatedReserves;
    }
}
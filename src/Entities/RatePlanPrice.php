<?php
namespace GRS\Entities;

class RatePlanPrice
{
    private $day;
    private $inventory;
    private $rackRate;
    private $dailyRate;
    private $grsRate;
    private $babyCotRackRate;
    private $babyCotDailyRate;
    private $babyCotGrsRate;
    private $extendBedRackRate;
    private $extendBedDailyRate;
    private $extendBedGrsRate;
    private $reservationState;
    private $minStay;
    private $maxStay;
    private $closed;

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return mixed
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * @param mixed $inventory
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * @return mixed
     */
    public function getRackRate()
    {
        return $this->rackRate;
    }

    /**
     * @param mixed $rackRate
     */
    public function setRackRate($rackRate)
    {
        $this->rackRate = $rackRate;
    }

    /**
     * @return mixed
     */
    public function getDailyRate()
    {
        return $this->dailyRate;
    }

    /**
     * @param mixed $dailyRate
     */
    public function setDailyRate($dailyRate)
    {
        $this->dailyRate = $dailyRate;
    }

    /**
     * @return mixed
     */
    public function getGrsRate()
    {
        return $this->grsRate;
    }

    /**
     * @param mixed $grsRate
     */
    public function setGrsRate($grsRate)
    {
        $this->grsRate = $grsRate;
    }

    /**
     * @return mixed
     */
    public function getBabyCotRackRate()
    {
        return $this->babyCotRackRate;
    }

    /**
     * @param mixed $babyCotRackRate
     */
    public function setBabyCotRackRate($babyCotRackRate)
    {
        $this->babyCotRackRate = $babyCotRackRate;
    }

    /**
     * @return mixed
     */
    public function getBabyCotDailyRate()
    {
        return $this->babyCotDailyRate;
    }

    /**
     * @param mixed $babyCotDailyRate
     */
    public function setBabyCotDailyRate($babyCotDailyRate)
    {
        $this->babyCotDailyRate = $babyCotDailyRate;
    }

    /**
     * @return int
     */
    public function getBabyCotGrsRate()
    {
        return $this->babyCotGrsRate;
    }

    /**
     * @param int babyCotGrsRate
     */
    public function setBabyCotGrsRate($babyCotGrsRate)
    {
        $this->babyCotGrsRate = $babyCotGrsRate;
    }

    /**
     * @return int
     */
    public function getExtendBedRackRate()
    {
        return $this->extendBedRackRate;
    }

    /**
     * @param int $extendBedRackRate
     */
    public function setExtendBedRackRate($extendBedRackRate)
    {
        $this->extendBedRackRate = $extendBedRackRate;
    }

    /**
     * @return int
     */
    public function getExtendBedDailyRate()
    {
        return $this->extendBedDailyRate;
    }

    /**
     * @param int $extendBedDailyRate
     */
    public function setExtendBedDailyRate($extendBedDailyRate)
    {
        $this->extendBedDailyRate = $extendBedDailyRate;
    }

    /**
     * @return int
     */
    public function getExtendBedGrsRate()
    {
        return $this->extendBedGrsRate;
    }

    /**
     * @param int $extendBedGrsRate
     */
    public function setExtendBedGrsRate($extendBedGrsRate)
    {
        $this->extendBedGrsRate = $extendBedGrsRate;
    }


    /**
     * @return string
     */
    public function getReservationState()
    {
        return $this->reservationState;
    }

    /**
     * @param string $reservationState
     */
    public function setReservationState($reservationState)
    {
        $this->reservationState = $reservationState;
    }

    /**
     * @return int
     */
    public function getMinStay()
    {
        return $this->minStay;
    }

    /**
     * @param int $minStay
     */
    public function setMinStay($minStay)
    {
        $this->minStay = $minStay;
    }

    /**
     * @return int
     */
    public function getMaxStay()
    {
        return $this->maxStay;
    }

    /**
     * @param int $maxStay
     */
    public function setMaxStay($maxStay)
    {
        $this->maxStay = $maxStay;
    }

    /**
     * @return boolean mixed
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * @param boolean $closed
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;
    }


    /**
     * @return array
     */
    public function toArray()
    {
        return get_object_vars( $this );
    }
}
<?php
namespace GRS\Entities;

class Profile
{
    private $name;
    private $debitAccountAmount;
    private $creditAccountAmount;
    private $webhook;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return integer
     */
    public function getDebitAccountAmount()
    {
        return $this->debitAccountAmount;
    }

    /**
     * @param integer $debitAccountAmount
     */
    public function setDebitAccountAmount($debitAccountAmount)
    {
        $this->debitAccountAmount = $debitAccountAmount;
    }

    /**
     * @return integer
     */
    public function getCreditAccountAmount()
    {
        return $this->creditAccountAmount;
    }

    /**
     * @param integer $creditAccountAmount
     */
    public function setCreditAccountAmount($creditAccountAmount)
    {
        $this->creditAccountAmount = $creditAccountAmount;
    }

    /**
     * @return string
     */
    public function getWebHook()
    {
        return $this->webhook;
    }

    /**
     * @param string $webhook
     */
    public function setWebHook($webhook)
    {
        $this->webhook = $webhook;
    }
}
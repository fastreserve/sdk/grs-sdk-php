<?php
namespace GRS\Entities;

class ReserveDetails
{
    private $propertyId;
    private $checkIn;
    private $checkOut;
    private $bookerFirstName;
    private $bookerLastName;
    private $bookerPhone;
    private $bookerEmail;
    private $vehicle;
    private $vehicleNumber;
    private $state;
    private $status;
    private $confirmationCode;
    private $propertyConfirmationCode;
    private $totalCancellationFee;
    private $totalModificationFee;
    private $totalPrice;
    private $totalDailyPrice;
    private $totalSalesPrice;
    private $description;
    private $createDate;
    private $expireDate;
    private $cancelDate;
    private $definiteDate;

    /**
     * @var ReserveRoomDetails[] $rooms
     */
    private $rooms;

    /**
     * @return integer
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param integer $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return string
     */
    public function getCheckIn()
    {
        return $this->checkIn;
    }

    /**
     * @param string $checkIn
     */
    public function setCheckIn($checkIn)
    {
        $this->checkIn = $checkIn;
    }

    /**
     * @return string
     */
    public function getCheckOut()
    {
        return $this->checkOut;
    }

    /**
     * @param string $checkOut
     */
    public function setCheckOut($checkOut)
    {
        $this->checkOut = $checkOut;
    }

    /**
     * @return string
     */
    public function getBookerFirstName()
    {
        return $this->bookerFirstName;
    }

    /**
     * @param string $bookerFirstName
     */
    public function setBookerFirstName($bookerFirstName)
    {
        $this->bookerFirstName = $bookerFirstName;
    }

    /**
     * @return string
     */
    public function getBookerLastName()
    {
        return $this->bookerLastName;
    }

    /**
     * @param string $bookerLastName
     */
    public function setBookerLastName($bookerLastName)
    {
        $this->bookerLastName = $bookerLastName;
    }

    /**
     * @return string
     */
    public function getBookerPhone()
    {
        return $this->bookerPhone;
    }

    /**
     * @param string $bookerPhone
     */
    public function setBookerPhone($bookerPhone)
    {
        $this->bookerPhone = $bookerPhone;
    }

    /**
     * @return string
     */
    public function getBookerEmail()
    {
        return $this->bookerEmail;
    }

    /**
     * @param string $bookerEmail
     */
    public function setBookerEmail($bookerEmail)
    {
        $this->bookerEmail = $bookerEmail;
    }

    /**
     * @return string
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * @param string $vehicle
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;
    }

    /**
     * @return string
     */
    public function getVehicleNumber()
    {
        return $this->vehicleNumber;
    }

    /**
     * @param string $vehicleNumber
     */
    public function setVehicleNumber($vehicleNumber)
    {
        $this->vehicleNumber = $vehicleNumber;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getConfirmationCode()
    {
        return $this->confirmationCode;
    }

    /**
     * @param string $confirmationCode
     */
    public function setConfirmationCode($confirmationCode)
    {
        $this->confirmationCode = $confirmationCode;
    }

    /**
     * @return string
     */
    public function getPropertyConfirmationCode()
    {
        return $this->propertyConfirmationCode;
    }

    /**
     * @param string $propertyConfirmationCode
     */
    public function setPropertyConfirmationCode($propertyConfirmationCode)
    {
        $this->propertyConfirmationCode = $propertyConfirmationCode;
    }

    /**
     * @return mixed
     */
    public function getTotalCancellationFee()
    {
        return $this->totalCancellationFee;
    }

    /**
     * @param mixed $totalCancellationFee
     */
    public function setTotalCancellationFee($totalCancellationFee)
    {
        $this->totalCancellationFee = $totalCancellationFee;
    }

    /**
     * @return mixed
     */
    public function getTotalModificationFee()
    {
        return $this->totalModificationFee;
    }

    /**
     * @param mixed $totalModificationFee
     */
    public function setTotalModificationFee($totalModificationFee)
    {
        $this->totalModificationFee = $totalModificationFee;
    }

    /**
 * @return integer
 */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param integer $totalPrice
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
    }

    /**
     * @return integer
     */
    public function getTotalDailyPrice()
    {
        return $this->totalDailyPrice;
    }

    /**
     * @param integer $totalDailyPrice
     */
    public function setTotalDailyPrice($totalDailyPrice)
    {
        $this->totalDailyPrice = $totalDailyPrice;
    }

    /**
     * @return integer
     */
    public function getTotalSalesPrice()
    {
        return $this->totalSalesPrice;
    }

    /**
     * @param integer $totalSalesPrice
     */
    public function setTotalSalesPrice($totalSalesPrice)
    {
        $this->totalSalesPrice = $totalSalesPrice;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param string $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return string
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * @param string $expireDate
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;
    }

    /**
     * @return string
     */
    public function getCancelDate()
    {
        return $this->cancelDate;
    }

    /**
     * @param string $cancelDate
     */
    public function setCancelDate($cancelDate)
    {
        $this->cancelDate = $cancelDate;
    }

    /**
     * @return string
     */
    public function getDefiniteDate()
    {
        return $this->definiteDate;
    }

    /**
     * @param string $definiteDate
     */
    public function setDefiniteDate($definiteDate)
    {
        $this->definiteDate = $definiteDate;
    }

    /**
     * @return ReserveRoomDetails[]
     */
    public function getRooms()
    {
        return $this->rooms;
    }

    /**
     * @param ReserveRoomDetails[] $rooms
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $properties = get_object_vars( $this );
        $properties['rooms'] = [];
        foreach ( $this->getRooms() as $reserveRoomDetails ){
            $properties['rooms'][] = $reserveRoomDetails->toArray();
        }
        return $properties;
    }
}
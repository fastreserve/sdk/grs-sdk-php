<?php
namespace GRS\Entities;

class PropertyDetails
{
    private $id;
    private $name;
    private $type;
    private $star;
    private $grade;
    private $provinceId;
    private $cityId;
    private $latitude;
    private $longitude;
    private $address;
    private $roomsCount;
    private $facilities;
    private $roomTypes;
    private $images;
    private $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getStar()
    {
        return $this->star;
    }

    /**
     * @param int $star
     */
    public function setStar($star)
    {
        $this->star = $star;
    }

    /**
     * @return string
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param string $grade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    }

    /**
     * @return int
     */
    public function getProvinceId()
    {
        return $this->provinceId;
    }

    /**
     * @param int $provinceId
     */
    public function setProvinceId($provinceId)
    {
        $this->provinceId = $provinceId;
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * @param int $cityId
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;
    }

    /**
     * @return double
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param double $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return double
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param double $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getRoomsCount()
    {
        return $this->roomsCount;
    }

    /**
     * @param int $roomsCount
     */
    public function setRoomsCount($roomsCount)
    {
        $this->roomsCount = $roomsCount;
    }

    /**
     * @return FacilityDetails[]
     */
    public function getFacilities()
    {
        return $this->facilities;
    }

    /**
     * @param FacilityDetails[] $facilities
     */
    public function setFacilities($facilities)
    {
        $this->facilities = $facilities;
    }

    /**
     * @return mixed
     */
    public function getRoomTypes()
    {
        return $this->roomTypes;
    }

    /**
     * @param mixed $roomTypes
     */
    public function setRoomTypes($roomTypes)
    {
        $this->roomTypes = $roomTypes;
    }

    /**
     * @return File[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param File[] $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $properties = get_object_vars( $this );
        $properties['facilities'] = [];
        foreach ( $this->getFacilities() as $facility ){
            $properties['facilities'][] = $facility->toArray();
        }
        return $properties;
    }
}
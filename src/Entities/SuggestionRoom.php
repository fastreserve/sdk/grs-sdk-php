<?php
namespace GRS\Entities;

class SuggestionRoom
{
    private $roomTypeId;
    private $roomTypeName;
    private $roomTypeCapacity;
    private $roomTypeExtraCapacity;
    private $ratePlans;


    /**
     * @return int
     */
    public function getRoomTypeId()
    {
        return $this->roomTypeId;
    }

    /**
     * @param int $roomTypeId
     */
    public function setRoomTypeId($roomTypeId)
    {
        $this->roomTypeId = $roomTypeId;
    }

    /**
     * @return string
     */
    public function getRoomTypeName()
    {
        return $this->roomTypeName;
    }

    /**
     * @param string $roomTypeName
     */
    public function setRoomTypeName($roomTypeName)
    {
        $this->roomTypeName = $roomTypeName;
    }

    /**
     * @return mixed
     */
    public function getRoomTypeCapacity()
    {
        return $this->roomTypeCapacity;
    }

    /**
     * @param mixed $roomTypeCapacity
     */
    public function setRoomTypeCapacity($roomTypeCapacity)
    {
        $this->roomTypeCapacity = $roomTypeCapacity;
    }

    /**
     * @return mixed
     */
    public function getRoomTypeExtraCapacity()
    {
        return $this->roomTypeExtraCapacity;
    }

    /**
     * @param mixed $roomTypeExtraCapacity
     */
    public function setRoomTypeExtraCapacity($roomTypeExtraCapacity)
    {
        $this->roomTypeExtraCapacity = $roomTypeExtraCapacity;
    }

    /**
     * @return RatePlan[]
     */
    public function getRatePlans()
    {
        return $this->ratePlans;
    }

    /**
     * @param RatePlan[] $ratePlans
     */
    public function setRatePlans($ratePlans)
    {
        $this->ratePlans = $ratePlans;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $roomRate = get_object_vars( $this );
        $roomRate['ratePlans'] = [];
        foreach ( $this->getRatePlans() as $ratePlan ) {
            $roomRate['ratePlans'][] = $ratePlan->toArray();
        }

        return $roomRate;
    }
}
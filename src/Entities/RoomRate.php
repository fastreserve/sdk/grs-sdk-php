<?php
namespace GRS\Entities;

class RoomRate
{
    private $propertyId;
    private $roomTypeId;
    private $roomTypeName;
    private $ratePlans;

    /**
     * @return int
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param int $propertyId
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
    }

    /**
     * @return int
     */
    public function getRoomTypeId()
    {
        return $this->roomTypeId;
    }

    /**
     * @param int $room_typeId
     */
    public function setRoomTypeId($roomTypeId)
    {
        $this->roomTypeId = $roomTypeId;
    }

    /**
     * @return string
     */
    public function getRoomTypeName()
    {
        return $this->roomTypeName;
    }

    /**
     * @param string $roomTypeName
     */
    public function setRoomTypeName($roomTypeName)
    {
        $this->roomTypeName = $roomTypeName;
    }

    /**
     * @return RatePlan[]
     */
    public function getRatePlans()
    {
        return $this->ratePlans;
    }

    /**
     * @param RatePlan $ratePlans
     */
    public function setRatePlans($ratePlans)
    {
        $this->ratePlans = $ratePlans;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $roomRate = get_object_vars( $this );
        $roomRate['ratePlans'] = [];
        foreach ( $this->getRatePlans() as $ratePlan ) {
            $roomRate['ratePlans'][] = $ratePlan->toArray();
        }

        return $roomRate;
    }
}
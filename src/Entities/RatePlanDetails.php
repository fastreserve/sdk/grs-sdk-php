<?php
namespace GRS\Entities;

class RatePlanDetails
{
    private $id;
    private $name;
    private $mealTypeIncluded;
    private $foodBoardType;
    private $breakfastRate;
    private $halfBoardRate;
    private $fullBoardRate;
    private $cancelable;
    private $sleeps;
    private $facilities;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMealTypeIncluded()
    {
        return $this->mealTypeIncluded;
    }

    /**
     * @param string $mealTypeIncluded
     */
    public function setMealTypeIncluded($mealTypeIncluded)
    {
        $this->mealTypeIncluded = $mealTypeIncluded;
    }

    /**
     * @return string
     */
    public function getFoodBoardType()
    {
        return $this->foodBoardType;
    }

    /**
     * @param string $foodBoardType
     */
    public function setFoodBoardType($foodBoardType)
    {
        $this->foodBoardType = $foodBoardType;
    }

    /**
     * @return int
     */
    public function getBreakfastRate()
    {
        return $this->breakfastRate;
    }

    /**
     * @param int $breakfastRate
     */
    public function setBreakfastRate($breakfastRate)
    {
        $this->breakfastRate = $breakfastRate;
    }

    /**
     * @return int
     */
    public function getHalfBoardRate()
    {
        return $this->halfBoardRate;
    }

    /**
     * @param int $halfBoardRate
     */
    public function setHalfBoardRate($halfBoardRate)
    {
        $this->halfBoardRate = $halfBoardRate;
    }

    /**
     * @return int
     */
    public function getFullBoardRate()
    {
        return $this->fullBoardRate;
    }

    /**
     * @param int $fullBoardRate
     */
    public function setFullBoardRate($fullBoardRate)
    {
        $this->fullBoardRate = $fullBoardRate;
    }

    /**
     * @return boolean
     */
    public function getCancelable()
    {
        return $this->cancelable;
    }

    /**
     * @param boolean $cancelable
     */
    public function setCancelable($cancelable)
    {
        $this->cancelable = $cancelable;
    }

    /**
     * @return int
     */
    public function getSleeps()
    {
        return $this->sleeps;
    }

    /**
     * @param int $sleeps
     */
    public function setSleeps($sleeps)
    {
        $this->sleeps = $sleeps;
    }

    /**
     * @return FacilityDetails[]
     */
    public function getFacilities()
    {
        return $this->facilities;
    }

    /**
     * @param FacilityDetails[] $facilities
     */
    public function setFacilities($facilities)
    {
        $this->facilities = $facilities;
    }
}
<?php
namespace GRS\Entities;

class Transaction
{
    private $reserveConfirmationCode;
    private $type;
    private $paidDate;
    private $amount;
    private $description;
    private $createdAt;

    /**
     * @return string
     */
    public function getReserveConfirmationCode()
    {
        return $this->reserveConfirmationCode;
    }

    /**
     * @param string $reserveConfirmationCode
     */
    public function setReserveConfirmationCode($reserveConfirmationCode)
    {
        $this->reserveConfirmationCode = $reserveConfirmationCode;
    }
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getPaidDate()
    {
        return $this->paidDate;
    }

    /**
     * @param \DateTime $paidDate
     */
    public function setPaidDate($paidDate)
    {
        $this->paidDate = $paidDate;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
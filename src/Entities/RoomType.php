<?php
namespace GRS\Entities;

class RoomType
{
    private $id;
    private $name;
    private $type;
    private $capacity;
    private $extraCapacity;
    private $singleBedCount;
    private $doubleBedCount;
    private $sofaBedCount;
    private $outOfService;
    private $facilities;
    private $ratePlans;
    private $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getExtraCapacity()
    {
        return $this->extraCapacity;
    }

    /**
     * @param int $extraCapacity
     */
    public function setExtraCapacity($extraCapacity)
    {
        $this->extraCapacity = $extraCapacity;
    }

    /**
     * @return int
     */
    public function getSingleBedCount()
    {
        return $this->singleBedCount;
    }

    /**
     * @param int $singleBedCount
     */
    public function setSingleBedCount($singleBedCount)
    {
        $this->singleBedCount = $singleBedCount;
    }

    /**
     * @return int
     */
    public function getDoubleBedCount()
    {
        return $this->doubleBedCount;
    }

    /**
     * @param int $doubleBedCount
     */
    public function setDoubleBedCount($doubleBedCount)
    {
        $this->doubleBedCount = $doubleBedCount;
    }

    /**
     * @param mixed $sofaBedCount
     */
    public function setSofaBedCount($sofaBedCount)
    {
        $this->sofaBedCount = $sofaBedCount;
    }

    /**
     * @return boolean
     */
    public function getOutOfService()
    {
        return $this->outOfService;
    }

    /**
     * @param boolean $outOfService
     */
    public function setOutOfService($outOfService)
    {
        $this->outOfService = $outOfService;
    }

    /**
     * @return mixed
     */
    public function getSofaBedCount()
    {
        return $this->sofaBedCount;
    }

    /**
     * @return FacilityDetails[]
     */
    public function getFacilities()
    {
        return $this->facilities;
    }

    /**
     * @param FacilityDetails[] $facilities
     */
    public function setFacilities($facilities)
    {
        $this->facilities = $facilities;
    }

    /**
     * @return RatePlan[]
     */
    public function getRatePlans()
    {
        return $this->ratePlans;
    }

    /**
     * @param RatePlan[] $ratePlans
     */
    public function setRatePlans($ratePlans)
    {
        $this->ratePlans = $ratePlans;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $roomType = get_object_vars( $this );
        $roomType['facilities'] = [];
        foreach ( $this->getFacilities() as $facility ){
            $roomType['facilities'][] = $facility->toArray();
        }
        return $roomType;
    }
}
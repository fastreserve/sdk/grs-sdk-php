<?php
namespace GRS\Exceptions;

class ReserveException extends BaseRuntimeException
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'ReserveException';
    }

}
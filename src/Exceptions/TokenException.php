<?php
namespace GRS\Exceptions;

class TokenException extends BaseRuntimeException
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'TokenException';
    }

}
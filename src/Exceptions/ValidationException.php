<?php
namespace GRS\Exceptions;

use GRS\Entities\Error;

class ValidationException extends BaseRuntimeException
{
    private $errors = [];
    /**
     * ValidationException constructor.
     * @param Error[] $errors
     */
    public function __construct( $errors )
    {
        $this->errors = $errors;
        parent::__construct('validation error');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ValidationException';
    }

    /**
     * @return Error[] $errors
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
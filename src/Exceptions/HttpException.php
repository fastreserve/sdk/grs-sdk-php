<?php 
namespace GRS\Exceptions;

class HttpException extends BaseRuntimeException
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'HttpException';
    }	
}
?>
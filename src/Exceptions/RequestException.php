<?php
namespace GRS\Exceptions;

class RequestException extends BaseRuntimeException
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'RequestException';
    }

}
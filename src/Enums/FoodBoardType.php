<?php
/**
 * Created by PhpStorm.
 * User: Bagheri
 * Date: 19/11/2018
 * Time: 02:18 PM
 */

namespace GRS\Enums;


class FoodBoardType extends Enum
{
    const LIMIT_OPTIONS = 'limit_options';
    const FULL_OPTIONS = 'full_options';
}
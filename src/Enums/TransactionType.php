<?php
/**
 * Class ${CLASS_NAME}
 *
 * @package kitkat
 * @subpackage Security
 */

namespace GRS\Enums;


class TransactionType extends Enum
{
    const INCREASE_AGENCY_DEBIT_ACCOUNT = 'increase_agency_debit_account';
    const WITHDRAW_AGENCY_DEBIT_ACCOUNT = 'withdraw_agency_debit_account';
}
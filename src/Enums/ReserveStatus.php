<?php
namespace GRS\Enums;


class ReserveStatus extends Enum
{
    const PENDING = 'pending';
    const BOOKING = 'booking';
    const BOOKED = 'booked';
    const DEFINITE = 'definite';
    const REJECTED = 'rejected';
    const SUGGESTED = 'suggested';
    const MODIFY_BOOKING = 'modify_booking';
    const REJECTED_MODIFY = 'rejected_modify';
    const MODIFYING = 'modifying';
    const MODIFIED = 'modified';
    const OVERBOOKING = 'overbooking';
    const REFUND = 'refund';
    const CANCELING = 'canceling';
    const CANCELED = 'canceled';
    const CANCELLATION_REJECTED = 'cancellation_rejected';
}
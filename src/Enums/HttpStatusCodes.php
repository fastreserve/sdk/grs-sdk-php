<?php
namespace GRS\Enums;

class HttpStatusCodes extends Enum
{
    // https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
    const OK = 200;
    const CREATE = 201;
    const NO_CONTENT = 204;

    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const PAYMENT_REQUIRED = 402;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const NOT_ACCEPTABLE = 406;
    const CONFLICT = 409;
    const MISDIRECTED_REQUEST = 421;
    const UNPROCESSABLE_ENTITY = 422;

    const INTERNAL_SERVER_ERROR = 500;
    const BAD_GATEWAY = 502;
}
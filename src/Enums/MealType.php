<?php
/**
 * Created by PhpStorm.
 * User: Bagheri
 * Date: 19/11/2018
 * Time: 02:16 PM
 */

namespace GRS\Enums;


class MealType extends Enum
{
    const BREAKFAST = 'breakfast';
    const HALF_BOARD = 'half_board';
    const FULL_BOARD = 'full_board';
}
<?php
/**
 * Class ${CLASS_NAME}
 *
 * @package kitkat
 * @subpackage Security
 */

namespace GRS\Enums;


class ReserveState extends Enum
{
    const ONLINE = 'online';
    const OFFLINE = 'offline';
}
<?php
/**
 * Class ${CLASS_NAME}
 *
 * @package kitkat
 * @subpackage Security
 */

namespace GRS\Enums;


class Vehicle extends Enum
{
    const BUS = 'bus';
    const TRAIN = 'train';
    const SHIP = 'ship';
    const AIRPLANE = 'airplane';
}
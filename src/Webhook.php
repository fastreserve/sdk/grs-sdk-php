<?php
namespace GRS;

use GRS\Exceptions\BaseRuntimeException;
use GRS\Factories\PropertyDetailsFactory;
use GRS\Factories\ReserveDetailsFactory;
use GRS\Factories\RoomRateFactory;

class Webhook
{
    const AVAILABLE_CHANGED = 'available_changed';
    const RESERVE_CHANGED = 'reserve_changed';
    const Property_CHANGED = 'property_changed';

    private $actions = [];
    private $body = null;

    /**
     * Webhook constructor.
     * @param \stdClass $body
     */
    function __construct($body)
    {
        $this->body = $body;
        if( empty( $this->body ) ){
            throw new BaseRuntimeException('request body is empty');
        }

        if( !isset( $this->body->method ) || !isset( $this->body->value ) ){
            throw new BaseRuntimeException('body is not valid');
        }
    }

    public function setAvailableChangedHandler(callable $handler)
    {
        $this->addAction(self::AVAILABLE_CHANGED, $handler);
    }

    public function setReserveChangedHandler(callable $handler)
    {
        $this->addAction(self::RESERVE_CHANGED, $handler);
    }

    public function setPropertyChangedHandler(callable $handler)
    {
        $this->addAction(self::Property_CHANGED, $handler);
    }

    public function handle()
    {
        switch ($this->getWebhookMethodName()) {
            case self::AVAILABLE_CHANGED:
                return $this->availableChangedResponse();
                break;
            case self::RESERVE_CHANGED:
                return $this->reserveChangedResponse();
                break;
            case self::Property_CHANGED:
                return $this->propertyChangedResponse();
                break;
        }
    }

    private function availableChangedResponse()
    {
        $handler = isset($this->actions[self::AVAILABLE_CHANGED]) ? $this->actions[self::AVAILABLE_CHANGED] : function () {
        };

        $roomRateCollection = (new RoomRateFactory())->makeFromArray($this->body->value);
        return call_user_func($handler, $roomRateCollection);
    }

    private function reserveChangedResponse()
    {
        $handler = isset($this->actions[self::RESERVE_CHANGED]) ? $this->actions[self::RESERVE_CHANGED] : function () {
        };

        $reserveDetails = (new ReserveDetailsFactory())->make($this->body->value);
        return call_user_func($handler, $reserveDetails);
    }

    private function propertyChangedResponse()
    {
        $handler = isset($this->actions[self::Property_CHANGED]) ? $this->actions[self::Property_CHANGED] : function () {
        };

        $propertyDetails = (new PropertyDetailsFactory())->make($this->body->value);
        return call_user_func($handler, $propertyDetails);
    }

    private function addAction($methodName, callable $handler)
    {
        $this->actions[$methodName] = $handler;
    }

    private function getWebhookMethodName()
    {
        return $this->body->method;
    }
}